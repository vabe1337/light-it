//
//  MainAPI.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class MainAPI: NSObject {
    
    static let shared = MainAPI()
    
    override private init() {}
    
    func registretionRequestWith(login:String, password:String, completion:@escaping (Bool?, String?) -> Void) {
        
        let params:[String: Any] = [
            "username" : login,
            "password": password
        ]
        
        request(Constants.baseURL.rawValue + "/api/register/", method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (responseJSON) in
            switch responseJSON.result {
            case .success:
                guard let value = responseJSON.result.value as? Dictionary<String, Any> else{
                    completion(nil, "Incorrect server response!")
                    return
                }
                guard value["success"] as? Bool == true else{
                    completion(nil, "Registration faild, please try again.")
                    return
                }
                print("RESPONSE POST /register/ : \n \(value)")
                completion(true,nil)
                break
            case .failure(let error):
                completion(nil,error.localizedDescription)
            }
        }
    }
    func loginRequestWith(login:String, password:String, completion:@escaping(Bool?, String?) ->Void){
        let params:[String: Any] = [
            "username" : login,
            "password": password
        ]
        request(Constants.baseURL.rawValue + "/api/login/", method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (responseJSON) in
            switch responseJSON.result {
            case .success:
                guard let value = responseJSON.result.value as? Dictionary<String, Any> else{
                    completion(nil, "Incorrect server response!")
                    return
                }
                guard value["success"] as? Bool == true else{
                    completion(nil, "Login faild, please try again.")
                    return
                }
                print("RESPONSE POST /login/ : \n \(value)")
                
                User.saveUser(user: User(json: value, name: login))
                
                completion(true,nil)
                break
            case .failure(let error):
                completion(nil,error.localizedDescription)
            }
        }
    }
    
    func getProductsWith(completion: @escaping (Array<Product>?, String?)->Void){
        
        request(Constants.baseURL.rawValue + "/api/products/").responseJSON { (responseJSON) in
            switch responseJSON.result {
            case .success:
                guard let products = responseJSON.result.value as? Array<Dictionary<String, Any>> else{
                    completion(nil, "Incorrect server response!")
                    return
                }
                print("RESPONSE GET /products/ : \n \(products)")
                var result:[Product] = []
                for productJSON in products{
                    result.append(Product(json: productJSON))
                }
                
                completion(result,nil)
                break
            case .failure(let error):
                completion(nil,error.localizedDescription)
            }
        }
        
    }
    func getImageWith(imageName:String, completion: @escaping (UIImage?, String?) -> Void){
        request(Constants.baseURL.rawValue + "/static/\(imageName)").responseImage(completionHandler: { (responseImage) in
            switch responseImage.result{
            case .success:
                guard let image = responseImage.result.value else {
                    completion(nil, "Error! Image not load!")
                    return
                }
                print("RESPONSE GET /image/: \(image))")
                completion(image, nil)
                break
            case .failure:
                completion(nil, "Error! Image not load!")
                break
            }
        })
        
    }
    
    func getReviewsRequestWith(productId:Int, completion:@escaping (Array<Review>?, String?) -> Void){
        request(Constants.baseURL.rawValue + "/api/reviews/\(productId)").responseJSON(completionHandler: { (responseJSON) in
            print(Constants.baseURL.rawValue + "/reviews/\(productId)")
            switch responseJSON.result{
            case .success:
                guard let value = responseJSON.result.value as? Array<Dictionary<String, Any>> else{
                    completion(nil, "Incorrect server response!")
                    return
                }
                var result:[Review] = []
                for review in value{
                    result.append(Review(json: review))
                }
                result = result.reversed()
                completion(result, nil)
                print("RESPONSE GET /reviews/ : \n \(value)")
                break
            case .failure:
                completion(nil, "Reviews not load!")
                break
            }
        })
    }
    
    
    func postReviewRequestWith(productId:Int, rate:Int, text:String, completion: @escaping (Bool?, String?) -> Void){
        
        
        do{
            let userData = UserDefaults.standard.data(forKey: Constants.kUser.rawValue)
            let decoder = JSONDecoder()
            let user = try decoder.decode(User.self, from: userData!) //NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(userData!) as! User
            let params:[String:Any] = [
                "rate": rate,
                "text": text
            ]
            guard  let token = user.token else {
                completion(nil, "Invalid token!")
                return
            }
            let headers:HTTPHeaders = ["Authorization": "Token \(token)"]
            
            request((Constants.baseURL.rawValue + "/api/reviews/\(productId)"), method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (responseJSON) in
                switch responseJSON.result {
                case .success:
                    guard let value = responseJSON.result.value as? Dictionary<String, Any> else{
                        completion(nil, "Incorrect server response!")
                        return
                    }
                    guard value["success"] as? Bool == true else{
                        completion(nil, "Faild post review. Please try again!")
                        return
                    }
                    completion(true, nil)
                    print("RESPONSE POST /reviews/ : \n \(value)")
                    
                    break
                case .failure(let error):
                    completion(nil,error.localizedDescription)
                }
            }
        }catch{
            print("Unarchiving error!")
        }
        
        
     
    }
}
