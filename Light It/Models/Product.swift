//
//  Product.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class Product: NSObject {
    var id:NSNumber!
    var title:String!
    var image:String!
    var text:String!
    
    
    
    init(json:Dictionary<String,Any>){
        
        self.id = json["id"] as? NSNumber ?? 0
        self.title = json["title"] as? String ?? ""
        self.image = json["img"] as? String ?? ""
        self.text = json["text"] as? String ?? ""
    }
}
