//
//  User.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import Foundation

struct User:Codable {
    
    
    var name:String!
    var picture:String?
    var token:String!
    
    
    init(json:Dictionary<String,Any>, name:String){
        
        self.name = name
        self.token = json["token"] as? String ?? ""
        
    }
    
    static func saveUser(user:User) {
        do{
            let encoder = JSONEncoder()
            let dataEncode = try encoder.encode(user)//NSKeyedArchiver.archivedData(withRootObject: user, requiringSecureCoding: true)
            UserDefaults.standard.set(dataEncode, forKey: Constants.kUser.rawValue)
            UserDefaults.standard.synchronize()
        }catch{
            print("Archiving error!")
        }
        
    }
}
