//
//  Reviews.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/31/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class Review: NSObject {
    
    var text:String?
    var rate:NSNumber! = 5
    var name:String! = ""
    var created_at:String!
    
    
    init(json:Dictionary<String, Any>){
        if let created_by =  json["created_by"] as? Dictionary<String, Any>{
            self.name = created_by["username"] as? String ?? ""
        }
        self.rate = json["rate"] as? NSNumber ?? 5
        self.text = json["text"] as? String ?? ""
        self.created_at = json["created_at"] as? String ?? "2013-12-19T00:00:00Z"
    }
}
