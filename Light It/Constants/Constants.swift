//
//  Constants.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//


enum Constants:String {
    case kIsLogin = "isLogin"
    case baseURL = "https://smktesting.herokuapp.com"
    case kUser = "User"
    case kName = "userName"
    case kSurname = "userSurname"
    case kAvatar = "userAvatar"
}
