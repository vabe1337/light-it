//
//  LoginProtocol.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

protocol LoginProtocol: NSObjectProtocol {
    func showAlertWith(title:String, message:String)
    func goToProductsViewController()
}
