//
//  LoginPresenter.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class LoginPresenter: NSObject {

    
    func login(login:String?, password:String?, view:LoginProtocol) {
        guard login != nil && login != "" else {
            view.showAlertWith(title:"Error!", message: "Please enter login!")
            return
        }
        guard password != nil && password != "" else {
            view.showAlertWith(title:"Error!", message: "Please enter password!")
            return
        }
        LoadingIndicator.showLoadingIndicator()
        MainAPI.shared.loginRequestWith(login: login!, password: password!) { (isLogin:Bool?, error:String?) in
            guard isLogin == true else{
                LoadingIndicator.hideLoadingIndicator()
                view.showAlertWith(title: "Error", message: error!)
                return
            }
            UserDefaults.standard.set(true, forKey: Constants.kIsLogin.rawValue)
            UserDefaults.standard.synchronize()
            view.goToProductsViewController()
            LoadingIndicator.hideLoadingIndicator()
        }
        
    }
    
}
