//
//  LoginViewController.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, LoginProtocol {

    

    @IBOutlet weak var passTxtField: UITextField!
    @IBOutlet weak var loginTxtField: UITextField!
    let presenter = LoginPresenter()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

 
    }
    
    @IBAction func login(_ sender: Any) {
    self.presenter.login(login: self.loginTxtField.text, password: self.passTxtField.text, view: self)
    
    }
    
    //MARK: - LoginProtocol
    
    func goToProductsViewController(){
        let storyboard = UIStoryboard(name: "Products", bundle: nil)
        let productsVc = storyboard.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
        self.navigationController?.pushViewController(productsVc, animated: true)
    }
    
    
    
    func showAlertWith(title:String, message: String) {
        self.present(AlertHelper.shared.showAlertWith(title:title, message: message), animated: true, completion: nil)
    }

}
