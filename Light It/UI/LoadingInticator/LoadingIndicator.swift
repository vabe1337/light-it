//
//  LoadingIndicator.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class LoadingIndicator: UIView {
    static var loadingIndicator:LoadingIndicator? = nil
    var indicator:UIActivityIndicatorView?
    var parentView:UIView?
    
    private init(parent:UIView?) {
        super.init(frame: CGRect.zero)
        if let view = parent {
            parentView = view
            self.frame = view.bounds
        }
        else {
            parentView = (UIApplication.shared.delegate?.window)!
            self.frame = UIScreen.main.bounds
        }
        self.backgroundColor = UIColor.clear
        
        let blurEffect = UIBlurEffect.init(style: UIBlurEffect.Style.dark)
        let blurView = UIVisualEffectView.init(effect: blurEffect)
        blurView.alpha = 0.5
        blurView.frame = self.frame
        self.addSubview(blurView)
        
        indicator = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.whiteLarge)
        indicator?.center = self.center
        self.addSubview(indicator!)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChanged(notification:)), name: UIDevice.orientationDidChangeNotification, object: UIDevice.current)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func orientationDidChanged(notification:NSNotification) {
        self.frame = (parentView?.bounds)!
        indicator?.center = self.center
    }
    
    class func showLoadingIndicator(parentView: UIView) {
        if (loadingIndicator == nil) {
            loadingIndicator = LoadingIndicator.init(parent: parentView)
            parentView.addSubview(loadingIndicator!)
        }
        loadingIndicator?.indicator?.startAnimating()
    }
    
    class func showLoadingIndicator() {
        if (loadingIndicator == nil) {
            loadingIndicator = LoadingIndicator.init(parent: nil)
            let window = UIApplication.shared.delegate?.window!
            window?.addSubview(loadingIndicator!)
        }
        loadingIndicator?.indicator?.startAnimating()
    }
    
    class func hideLoadingIndicator() {
        if (loadingIndicator != nil) {
            loadingIndicator?.indicator?.stopAnimating()
            loadingIndicator?.removeFromSuperview()
            loadingIndicator = nil
        }
        
    }
}
