//
//  ReviewView.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/31/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ReviewView: UIView, ReviewProtocol {

    var rate:Int = 0
    var productId:Int!
    var productInfoDelegate:ProductInfoDelegate!
    let presenter = ReviewPresenter()
    @IBOutlet weak var starsStackView: UIStackView!

    @IBOutlet weak var commentTxtView: UITextView!
    
    @IBAction func ratePressed(_ sender: UIButton) {
        self.presenter.setupStarsWith(button:sender, starsStackView:self.starsStackView, view:self)
    }

    @IBAction func onTuchSubmitBtn(_ sender: Any) {
       self.presenter.submitWith(comment:self.commentTxtView, view:self, productDelegate: self.productInfoDelegate)
        
    }

    @IBAction func onTuchExit(_ sender: Any) {
        self.removeView()
    }
    
    
    func removeView()  {
        self.removeFromSuperview()
    }
    
}
