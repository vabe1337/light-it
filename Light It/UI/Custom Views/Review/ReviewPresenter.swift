//
//  ReviewPresenter.swift
//  Light It
//
//  Created by Ivan Nezdropa on 9/1/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ReviewPresenter: NSObject {
    func setupStarsWith(button:UIButton, starsStackView:UIStackView, view:ReviewProtocol){
        view.rate = button.tag
        for view in starsStackView.subviews{
            let starBtn = view as! UIButton
            if starBtn.tag <= button.tag{
                starBtn.setBackgroundImage(UIImage(named: "star_selected"), for: UIControl.State.normal)
            }else{
                starBtn.setBackgroundImage(UIImage(named: "star_unselected"), for: UIControl.State.normal)
            }
        }
    }
    func submitWith(comment:UITextView, view:ReviewProtocol, productDelegate: ProductInfoDelegate){
        let isLogin = UserDefaults.standard.bool(forKey: Constants.kIsLogin.rawValue)
        
        if !isLogin {
            productDelegate.showAlertWith(title: "Error!", message: "Plase login and try again!")
            return
        }
        
        guard comment.text != "" else{
            productDelegate.showAlertWith(title: "Error!", message: "Please enter comment!")
            return
        }
        
        
        LoadingIndicator.showLoadingIndicator()
        MainAPI.shared.postReviewRequestWith(productId: view.productId, rate: view.rate, text: comment.text) { (success:Bool?, error:String?) in
            
            guard  success != nil else{
                
                view.removeView()
                
                productDelegate.showAlertWith(title:"Error", message:error!)
                
                LoadingIndicator.hideLoadingIndicator()
                
                return
            }
            
            view.removeView()
            
            productDelegate.refreshReviewsTable()
            
            LoadingIndicator.hideLoadingIndicator()
        }
    }
}
