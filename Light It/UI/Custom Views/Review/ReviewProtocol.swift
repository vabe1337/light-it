//
//  ReviewProtocol.swift
//  Light It
//
//  Created by Ivan Nezdropa on 9/1/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

protocol ReviewProtocol: NSObjectProtocol {
    var rate:Int {get set}
    var productId:Int! {get set}
    var productInfoDelegate:ProductInfoDelegate! {get set}
    func removeView()
}
