//
//  RegistrationPresenter.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class RegistrationPresenter: NSObject {
    
    func registerWith(login:String?, password:String?, view:RegistrationProtocol, confirmPassword:String?){
        guard let login = login else {
            view.showAlertWith(title:"Error!", message: "Please enter login!")
            return
        }
        guard let password = password else {
            view.showAlertWith(title:"Error!", message: "Please enter password!")
            return
        }
        guard let confirmPassword = confirmPassword else {
            view.showAlertWith(title:"Error!", message: "Please enter confirmPassword!")
            return
        }
        
        guard login != "" || password != "" || confirmPassword != "" else{
            view.showAlertWith(title:"Error!", message: "Please enter valid credentials!")
            return
        }
        guard password == confirmPassword else {
            view.showAlertWith(title:"Error!", message: "Passwords do not match!")
            return
        }
        
        LoadingIndicator.showLoadingIndicator()
        MainAPI.shared.registretionRequestWith(login:login, password:password, completion:{(success:Bool?, error:String?) in
            guard success != nil else{
                LoadingIndicator.hideLoadingIndicator()
                view.showAlertWith(title: "Error!", message: error!)
                return
            }
            view.showPreviousViewController()
            LoadingIndicator.hideLoadingIndicator()
        })
    }
    
}
