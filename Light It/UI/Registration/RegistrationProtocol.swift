//
//  RegistrationProtocol.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import Foundation

protocol RegistrationProtocol:NSObjectProtocol {
    func showAlertWith(title:String, message:String)
    func showPreviousViewController()
}
