//
//  RegistrationViewController.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/26/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, RegistrationProtocol {

    
    let presenter = RegistrationPresenter()
    @IBOutlet weak var loginTxtField: UITextField!
    @IBOutlet weak var passTxtField: UITextField!
    @IBOutlet weak var confirmPassTxtField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.confirmPassTxtField.addTarget(self, action: #selector(onChangeConfirmPassword), for: UIControl.Event.editingChanged)
    }
    
    @IBAction func register(_ sender: Any) {
        self.presenter.registerWith(login: self.loginTxtField.text, password: self.passTxtField.text,view: self, confirmPassword: self.confirmPassTxtField.text)
    }
    

    @objc func onChangeConfirmPassword() {
        if self.confirmPassTxtField.text != self.passTxtField.text {
            self.confirmPassTxtField.layer.borderWidth = 2.0
            self.confirmPassTxtField.layer.borderColor = UIColor.red.cgColor
        }else{
            self.confirmPassTxtField.layer.borderWidth = 0.0
            self.confirmPassTxtField.layer.borderColor = UIColor.clear.cgColor

        }
    }
    
    
    //MARK: - RegistrationProtocol
    func showAlertWith(title:String, message:String){
        self.present(AlertHelper.shared.showAlertWith(title:title, message: message), animated: true, completion: nil)
    }
    
    func showPreviousViewController(){
        self.navigationController?.popViewController(animated: true)
    }
}
