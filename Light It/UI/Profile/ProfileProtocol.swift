//
//  ProfileProtocol.swift
//  Light It
//
//  Created by Ivan Nezdropa on 9/1/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

protocol ProfileProtocol: NSObjectProtocol {
    func showAlertWith(title:String, message:String)
}
