//
//  ProfilePresenter.swift
//  Light It
//
//  Created by Ivan Nezdropa on 9/1/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ProfilePresenter: NSObject {
    func getProfileDataWith(nameTxTField:UITextField?, surnameTxTField:UITextField?, avatarBtn:UIButton){
        
        let name = UserDefaults.standard.string(forKey: Constants.kName.rawValue) ?? ""
        let surname = UserDefaults.standard.string(forKey: Constants.kSurname.rawValue) ?? ""
        if let avatarData = UserDefaults.standard.data(forKey: Constants.kAvatar.rawValue) {
            avatarBtn.setBackgroundImage(UIImage(data: avatarData), for: UIControl.State.normal)
        }else{
            avatarBtn.setBackgroundImage(UIImage(named: "no-profile"), for: UIControl.State.normal)
        }
        
        nameTxTField?.text = name
        surnameTxTField?.text = surname
        
    }
    
    
    func saveUserProfileDataWith(nameTxTField:UITextField?, surnameTxTField:UITextField?, avatarImg:UIImage, view:ProfileProtocol){
        
        guard nameTxTField?.text != "" else {
            view.showAlertWith(title: "Error!", message: "Please enter your name!")
            return
        }
        guard surnameTxTField?.text != "" else {
            view.showAlertWith(title: "Error!", message: "Please enter your surname!")
            return
        }
        UserDefaults.standard.set(nameTxTField?.text, forKey: Constants.kName.rawValue)
        UserDefaults.standard.set(surnameTxTField?.text, forKey: Constants.kSurname.rawValue)
        let data:NSData = NSData(data: avatarImg.jpegData(compressionQuality: 1)!)
        UserDefaults.standard.set(data , forKey: Constants.kAvatar.rawValue)
        UserDefaults.standard.synchronize()
    }
}
