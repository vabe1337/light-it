//
//  ProfileViewController.swift
//  Light It
//
//  Created by Ivan Nezdropa on 9/1/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ProfileProtocol {

    let presenter = ProfilePresenter()
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var surnameTxtField: UITextField!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var avatarBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.getProfileDataWith(nameTxTField:self.nameTxtField, surnameTxTField: self.surnameTxtField, avatarBtn: self.avatarBtn)
      
    }
    
    @IBAction func avatarPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func savePressed(_ sender: Any) {
        self.presenter.saveUserProfileDataWith(nameTxTField: self.nameTxtField, surnameTxTField: self.surnameTxtField, avatarImg: self.avatarBtn.backgroundImage(for: UIControl.State.normal)!, view: self)
    }
    
  
    
    
    func showAlertWith(title:String, message:String){
        self.present(AlertHelper.shared.showAlertWith(title:title, message: message), animated: true, completion: nil)
    }
    
    
    //MARK: - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            self.avatarBtn.setBackgroundImage(image, for: UIControl.State.normal)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
