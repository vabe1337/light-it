//
//  ProductsPresenter.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/30/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ProductsPresenter: NSObject {

    
    func getAvatar() -> UIImage? {
        if let avatarData = UserDefaults.standard.data(forKey: Constants.kAvatar.rawValue) {
            return UIImage(data: avatarData)
        }else{
            return UIImage(named: "no-profile")
        }
    }
    
    func getProducts(view:ProductsProtocol){
        LoadingIndicator.showLoadingIndicator()
        MainAPI.shared.getProductsWith(completion: { (products:Array<Product>?, error:String?) in
            guard let products = products else{
                LoadingIndicator.hideLoadingIndicator()
                view.showAlertWith(title:"Error!", message: error ?? "Server error! Products no found!")
                return
            }
            view.products = products
            view.numberOfRows = products.count
            view.updateTable()
            
            LoadingIndicator.hideLoadingIndicator()
            
        })
    }
    func setImageForProduct(name:String, imageView:UIImageView){
        //if the image is not loaded, it will be white
        MainAPI.shared.getImageWith(imageName: name, completion: { (image:UIImage?, error:String?) in
            imageView.image = image
        })
    }
    
    func isLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.kIsLogin.rawValue)
    }
    func signOut() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
    
}
