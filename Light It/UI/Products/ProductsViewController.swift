//
//  ProductsViewController.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/30/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, ProductsProtocol  {
    
    
    let presenter = ProductsPresenter()
    var numberOfRows:Int = 0
    var products:[Product] = []
    @IBOutlet weak var productsTableView: UITableView!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productsTableView.delegate = self
        self.productsTableView.dataSource = self
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.getProducts(view: self)
            if !presenter.isLogin(){
            self.loginBtn.addTarget(self, action: #selector(openLoginVc), for: .touchUpInside)
            self.loginBtn.setTitle("Login", for: UIControl.State.normal)
        }else{
            self.setUpMenuButton()
            self.loginBtn.addTarget(self, action: #selector(signOut), for: .touchUpInside)
            self.loginBtn.setTitle("Sign Out", for: UIControl.State.normal)
        }
    }
    
    
    
    
    func setUpMenuButton(){
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.setImage(self.presenter.getAvatar(), for: .normal)
        menuBtn.addTarget(self, action: #selector(openProfile), for: UIControl.Event.touchUpInside)
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        var size:CGFloat
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad{
            size = CGFloat(40)
        }else{
            size = CGFloat(30)
        }
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: size)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: size)
        currHeight?.isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem
    }
    
    @objc func openProfile(){
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let profileVc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(profileVc, animated: true)
        
    }
    
    
    
    

    
    @objc func openLoginVc(){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let loginVc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVc, animated: true)
    }
    
    @objc func signOut(){
        presenter.signOut()
        self.loginBtn.addTarget(self, action: #selector(openLoginVc), for: .touchUpInside)
        self.loginBtn.setTitle("Login", for: UIControl.State.normal)
        self.navigationItem.rightBarButtonItem = nil
    }
    
    //MARK: - ProductsProtocol
    func updateTable() {
        self.productsTableView.reloadData()
    }

    
    func showAlertWith(title:String, message:String){
        self.present(AlertHelper.shared.showAlertWith(title:title, message: message), animated: true, completion: nil)
    }
    
}





extension ProductsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath)
        
        guard let productImage = cell.viewWithTag(101) as? UIImageView else{
            return cell
        }
        
        guard let productTitle = cell.viewWithTag(102) as? UILabel else{
            return cell
        }
        
        
        let product = self.products[indexPath.row]
        productTitle.text = product.title
        self.presenter.setImageForProduct(name: product.image!, imageView: productImage)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad{
            return 210.0
        }else{
            return 100.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "ProductInfo", bundle: nil)
        let product = self.products[indexPath.row]
        let productInfoVc = storyboard.instantiateViewController(withIdentifier: "ProductInfoViewController") as! ProductInfoViewController
            productInfoVc.imageName = product.image!
            productInfoVc.titleLbl = product.title!
            productInfoVc.productId = product.id.intValue
            productInfoVc.productDescription = product.text!
            self.navigationController?.pushViewController(productInfoVc, animated: true)
        
       
    }
    
}
