//
//  ProductsProtocol.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/30/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

protocol ProductsProtocol: NSObjectProtocol {
    var numberOfRows:Int {get set}
    func updateTable()
    var products:[Product] {get set}
    func showAlertWith(title:String, message:String)
}
