//
//  ProductInfoViewController.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/30/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ProductInfoViewController: UIViewController, ProductInfoProtocol, ProductInfoDelegate {
  
    

    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descTxtView: UITextView!
    @IBOutlet weak var reviewsTblView: UITableView!
    
    let presenter = ProductInfoPresenter()
    var imageName:String!
    var titleLbl:String!
    var productId:Int!
    var productDescription:String = ""
    var reviews:[Review] = []
    var numberOfRows:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.setImageForProduct(name:imageName, imageView:self.image)
        self.titleLabel.text = self.titleLbl
        self.descTxtView.text = self.productDescription
        self.reviewsTblView.delegate = self
        self.reviewsTblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.getReviewsWith(productId: self.productId, view:self)
    }
    
    
    @IBAction func onTuchReviewButton(_ sender: Any) {
        if let view = Bundle.main.loadNibNamed("ReviewView", owner: self, options: nil)?.first as? ReviewView{
            let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            view.frame = frame
            view.productId = self.productId
            view.productInfoDelegate = self
            self.view.addSubview(view)
        }
    }
    
    
    
    //MARK: - ProductInfoProtocol, ProductInfoDelegate

    
    func showAlertWith(title:String, message:String){
        self.present(AlertHelper.shared.showAlertWith(title:title, message: message), animated: true, completion: nil)
    }
    
    func refreshReviewsTable(){
        self.presenter.getReviewsWith(productId: self.productId, view:self)
        self.reviewsTblView.reloadData()
    }
    
    func updateTable(){
        self.reviewsTblView.reloadData()
    }
    
    
    
    private func setStarsWith(stackView: UIStackView, rate:Int){
        for view in stackView.subviews{
            let starBtn = view as! UIButton
            if starBtn.tag <= rate{
                starBtn.setBackgroundImage(UIImage(named: "star_selected"), for: UIControl.State.normal)
            }else{
                starBtn.setBackgroundImage(UIImage(named: "star_unselected"), for: UIControl.State.normal)
            }
        }
    }
}


extension ProductInfoViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad{
            return 350.0
        }else{
            return 150.0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath)
        
        let review = self.reviews[indexPath.row]
        
        
        guard let stackView = cell.viewWithTag(102) as? UIStackView else {
            return cell
        }
        guard let nameLabel = cell.viewWithTag(101) as? UILabel else{
            return cell
        }
        guard let commentTxtView = cell.viewWithTag(103) as? UITextView else{
            return cell
        }
        guard let timeLabel = cell.viewWithTag(104) as? UILabel else{
            return cell
        }
        
        self.setStarsWith(stackView: stackView, rate: review.rate.intValue)
        nameLabel.text = review.name
        commentTxtView.text = review.text
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let oldDate = dateFormater.date(from: review.created_at)
        dateFormater.dateFormat = "dd/MM/yy, HH:mm"
        if let oldDate = oldDate {
            timeLabel.text = dateFormater.string(from: oldDate)
            return cell
        }
        return cell
    }
    
    
}
