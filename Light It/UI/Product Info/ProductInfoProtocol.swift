//
//  ProductInfoProtocol.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/31/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

protocol ProductInfoProtocol: NSObjectProtocol {
    var reviews:[Review] {get set}
    func showAlertWith(title:String, message:String)
    func updateTable()
    var numberOfRows:Int {get set}
}
