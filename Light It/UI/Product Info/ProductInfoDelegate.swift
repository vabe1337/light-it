//
//  ProductInfoDelegate.swift
//  Light It
//
//  Created by Ivan Nezdropa on 9/1/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

protocol ProductInfoDelegate: NSObjectProtocol {
    func refreshReviewsTable()
    func showAlertWith(title:String, message:String)
}
