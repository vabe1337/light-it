//
//  ProductInfoPresenter.swift
//  Light It
//
//  Created by Ivan Nezdropa on 8/30/19.
//  Copyright © 2019 My. All rights reserved.
//

import UIKit

class ProductInfoPresenter: NSObject {
    func setImageForProduct(name:String, imageView:UIImageView){
        //if the image is not loaded, it will be white
        MainAPI.shared.getImageWith(imageName: name, completion: { (image:UIImage?, error:String?) in
            imageView.image = image
        })
    }
    
    
    func getReviewsWith(productId: Int, view: ProductInfoProtocol){
        LoadingIndicator.showLoadingIndicator()
        MainAPI.shared.getReviewsRequestWith(productId: productId) { (reviews:Array<Review>?, error:String?) in
            guard let reviews = reviews else{
                LoadingIndicator.hideLoadingIndicator()
                view.showAlertWith(title: "Error", message: "Reviews not load")
                return
            }
            view.reviews = reviews
            view.numberOfRows = reviews.count
            view.updateTable()
            LoadingIndicator.hideLoadingIndicator()
        }
    }
}
